# Adrien

sed : récupération du premier match :
```
sed -e 's/truc/muche&/g' bidule.texte
>>muchetruc
```

OOM killer : soft qui tue les programmes les plus consommateurs en RAM lorsque cette dernière sature :
Voir le score :
```
printf 'PID\tOOM Score\tOOM Adj\tCommand\n'
while read -r pid comm; do [ -f /proc/$pid/oom_score ] && [ $(cat /proc/$pid/oom_score) != 0 ] && printf '%d\t%d\t\t%d\t%s\n' "$pid" "$(cat /proc/$pid/oom_score)" "$(cat /proc/$pid/oom_score_adj)" "$comm"; done < <(ps -e -o pid= -o comm=) | sort -k 2nr
```

Dans quel fichier j'ai parlé du oom_score déjà ?
```
grep -riHn oom_score 2>/dev/null
Extra2-Usefull_commands/administration.md:13:while read -r pid comm; do [ -f /proc/$pid/oom_score ] && [ $(cat /proc/$pid/oom_score) != 0 ] && printf '%d\t%d\t\t%d\t%s\n' "$pid" "$(cat /proc/$pid/oom_score)" "$(cat /proc/$pid/oom_score_adj)" "$comm"; done < <(ps -e -o pid= -o comm=) | sort -k 2nr
```
ah oui ! dans la ligne 13 de ce même fichier ;)
