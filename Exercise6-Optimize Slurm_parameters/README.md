# Optimize slurm parameters
Optimizing SLURM parameters is paramount for multiple reasons:
* To not be overcharged, you pay what you were **allocated** and not what you **used**
* To minimize waiting time, keep in mind that you are using a ditributed cluster with hundreds of people
* To not get cancelled, if you don't book enough ressources and your program tries to use more, it will get killed

Keep in mind that your SLURM allocation should be as close as possible from the real ressources used by your program. BUT, you should always leave some leeway.

If your program uses 2,5GB of RAM and will run for 1H20min =>
```bash
#SBATCH --time=01:30:00     # Running time + leeway
#SBATCH --mem=3G           # RAM allocated + 500MB of leeway
```

If you don't know how much your program use, you can remove all parameters and use the exclusive option in SLURM:
```bash
#SBATCH --exclusive
```

This option means that you will take all the ressources of a single node. Once finished, monitor RAM, CPU and time spent underload with the commands below.

## Monitoring RAM, CPU usage and time spent underload
Once your job is done, you can use the "seff" command to display information about the ressources used by it.

The syntax is the following:
```bash
seff <JOB-ID>     # JOBID is the unique identifier of your job
```

If you don't remember your JOBID, you can use the command "sacct" to list all past job:
```bash
(base) [verrierj@muse-login02 ~]$ sacct
JobID           JobName  Partition    Account  AllocCPUS      State ExitCode
------------ ---------- ---------- ---------- ---------- ---------- --------
9739443        hostname       defq     irstea          1 CANCELLED+      0:0
9739594        hostname       defq     irstea          1 CANCELLED+      0:0
9739612        hostname      fmuse     irstea          1 CANCELLED+      0:0
9739614        hostname      fmuse     f_meso          1  COMPLETED      0:0
9739765          mpi_pi      fmuse     f_meso          1 CANCELLED+      0:0
9739937          mpi_pi      fmuse     f_meso          1  COMPLETED      0:0
9740097          mpi_pi      fmuse     f_meso          1  COMPLETED      0:0
9740343          mpi_pi      fmuse     f_meso          1  COMPLETED      0:0
```

Seff is very easy to read:
```bash
(base) [verrierj@muse-login02 ~]$ seff 9740343
Job ID: 9740343                                   # JOBID
Cluster: cluster                                  # Cluster name
User/Group: verrierj/irstea                       # User and group name
State: COMPLETED (exit code 0)                    # Exit code
Cores: 1                                          # Cores allocated
CPU Utilized: 00:00:20                            # Time spent computing
CPU Efficiency: 95.24% of 00:00:21 core-walltime  # 95% of the time allocated was used to compute
Job Wall-clock time: 00:00:21                     # 21 seconds is the total allocation
Memory Utilized: 6.46 MB                          # Program used 6.46MB of RAM
Memory Efficiency: 0.16% of 4.00 GB               # 0.16% of 4GB has been used
```

## Optimizing the number of CPUs and nodes
Reminder, a thread on Meso@LR is a core. If you have 2 nodes with 28 cores allocated, you have 56 threads at your disposal.

The number of cores (threads) to have the most optimized execution time of your program is heavily dependant on how it works. Some programs can benefit from having more threads allocated but it is not the case for all of them. Even if your program is multi-threaded, there is a limit about how much threads it can uses before it is either not faster (more informations here: https://en.wikipedia.org/wiki/Amdahl's_lawexecution) or even slower in some cases!

The best you can do is testing multiple parameters and monitor the time your program spent computing. We strongly suggest you to do that with a small dataset as an input (if you can) because you will have to do a lot of testing.

## Exercise

Use the sacct command to retrieve the JOBID of your past PI job and display all informations about ressources usage with seff.

Now, refactor [pi.sh](../Exercise5-Computing_PI/pi.sh) t be as close as possible from the real ressources usage.

Answers are in [pi-optimized.sh](pi-optimized.sh)

