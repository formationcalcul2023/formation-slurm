# Slurm Architecture
You are now connected to the login nodes and wants to use the computing nodes of Meso@LR. You cannot interact with them directly but you have to use a tool called SLURM.

## SLURM, Introduction
SLURM stands for Simple Linux Utility for Resource Management. It is called a job scheduler and it is the one granting you access to computing nodes.

Think of it has a parking lot with mutliple sized cars and trucks. The goals is to fit them all into the available spots. If no one is guiding cars, this happens ![parking without slurm](./parking-disaster.png?raw=true)


As you can see, not many spaces left because no one tell them where to go in an optimized manner. It's basically a loss of ressources for example, if a server is used at 2% CPU then 98% could be used by another program.

This is what slurm does, basically =>
![parking with slurm](./parking-by-slurm.png?raw=true)

It optimizes every program to fit as many program as possible at a given time and minimize waiting time (if the queue is full because it's mismanaged). 

But it does that with MANY, MANY parameters such as (non exhaustive list)
* Duration of the job
* Available Memory
* Requested Memory
* Number of nodes requested
* Number of nodes available
* Number of cores requested
* Number of cores available
* Number of jobs submitted by the user since the beginning of times
* Weight of the nodes
* [Many many mores ...](https://slurm.schedmd.com/SUG14/sched_tutorial.pdf)

## Partition

A slurm partition is a queue with parameters and a node list. If you don't pay for an exclusive node, only "defq" is available to you which contains all the nodes in the public pool.

defq is the default queue where all your jobs will land. There is no time limit and all 94 nodes in it have the same configuration (28 cores, 128 GO of memory).

### Exercises

Connect to the cluster [How to connect to the cluster](../Exercise1-Connect_to_MesoLR/) and launch the following commands on the cluster=>
```bash
sinfo -l
```
What do you see?

sinfo allows you to have a quick look about the partition availables and their states. You can see how many nodes are available at a give time.

```bash
[verrierj@muse-login02 ~]$ sinfo -l
Thu Jun 09 15:36:21 2022
PARTITION AVAIL  TIMELIMIT   JOB_SIZE ROOT OVERSUBS     GROUPS  NODES       STATE NODELIST
defq*        up   infinite 1-infinite   no       NO admin,cvsu      8       mixed muse[101-102,104-106,108-109,111]
defq*        up   infinite 1-infinite   no       NO admin,cvsu      8   allocated muse[097-100,103,107,112-113]
defq*        up   infinite 1-infinite   no       NO admin,cvsu     78        idle muse[110,114-190]
fmuse        up   infinite 1-infinite   no       NO admin,f_me     10        idle muse[080-089]
```
What do you think the following states stand for?
<details>
<summary>idle</summary>
Node is available and waiting for a job
</details>
<details>
<summary>allocated</summary>
Node is busy and full running another job (or mutiple small jobs)
</details>
<details>
<summary>Mixed</summary>
Node is busy but still has room for another job
</details>

If you need more information about a partition, you can type the following command =>
```bash
scontrol show partition defq
```

Check if your group is in the "AllowGroups" of the partition by comparing the outpout of the "id" command =>
```bash
[verrierj@muse-login02 ~]$ id
uid=1065(verrierj) gid=1107(irstea) groups=1107(irstea),1739(admincloud),1795(f_meso),1819(inrae-infra)
```

Your group is important and must be specified when launching a job [See exercise 4](../Exercise%204%20-%20My%20first%20job/).

## Queue state 
The command "squeue" give you more information about what is going on in the queue at a given time.

First try to launch it and let's analyse the results together
```bash 
[verrierj@muse-login02 ~]$ squeue

JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
8088788      defq       cs   tabutm PD       0:00      1 (Priority)
8035807      defq bcpz.mus wozniakh  R 2-06:30:56      1 muse103
8036049      defq bcpz.mus wozniakh  R 2-04:21:29      1 muse107
8048709      defq      bwa    pozog  R 1-02:44:02      1 muse097
8049277      defq 50-100_P taracona  R   14:12:59      3 muse[098-100]
8075969      defq      abc   tabutm  R    3:54:30      1 muse111
8075970      defq      abc   tabutm  R    3:53:52      1 muse101
8075972      defq      abc   tabutm  R    3:53:22      1 muse108
8075975      defq      abc   tabutm  R    3:51:30      1 muse109
8077661      defq      abc   tabutm  R    1:35:30      1 muse102
8077662      defq      abc   tabutm  R    1:34:54      1 muse104
8077663      defq      abc   tabutm  R    1:34:30      1 muse105
8077665      defq      abc   tabutm  R    1:34:24      1 muse106
[verrierj@muse-login02 ~]$

```
We can see mutliple fields that could be important
```bash
JOBID      # the unique jobid of a program
Partition  # on which partition the job was launched
Name       # An arbitrary name set by the programmer of the job
User       # the user that launched it
ST         # stand for State PD means pending, R Running, F Failed, CF Configuring
TIME       # Time elapsed since the start of the job
NODES      # Number of nodes
NODELIST   # list of the nodes where the job is computing
```

The command has multiple options the most usefull one is "-u". It allows you to print only informations about jobs launched by a specific username.

Do the following command on Meso@LR after launching a job to see his state => 
```bash 
squeue -u $USER
```
## Accounting
Has you may know, Meso@LR cluster is not free. But how do you know how much of your quota you used? And waht if you don't remmeber how much hour you paid for?

You can use the "mon_nbre_heures" command to get this information. It will ask you for your group name and once entered, you will have informations like this =>
```bash
[verrierj@muse-login02 ~]$ mon_nbre_heures
Quel est le nom de votre groupe ? (Si vide, groupe principal)
inrae-infra
Le groupe inrae-infra possède un quota de 1521000 heures
L'ensemble du groupe a utilisé 82296 heures
Vous avez consommé au sein de ce groupe 53 heures
```

